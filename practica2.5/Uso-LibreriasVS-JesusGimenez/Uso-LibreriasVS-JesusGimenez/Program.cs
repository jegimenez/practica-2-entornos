﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibreriaVS_JesusGimenez;

namespace Uso_LibreriasVS_JesusGimenez
{
    class Program
    {
        static void Main(string[] args)
        {
                System.Console.WriteLine(Class1.EsPerfecto(15));
                System.Console.WriteLine(Class1.Factores(15));
                System.Console.WriteLine(Class1.maximo(10, 45));
                System.Console.WriteLine(Class1.minimo(15, 30));
                System.Console.WriteLine(Class2.redondear(15));
                System.Console.WriteLine(Class2.redondearAlza(15));
                System.Console.WriteLine(Class2.redondearBaja(15));
                System.Console.WriteLine(Class2.absoluto(15));
            }
    }
}
