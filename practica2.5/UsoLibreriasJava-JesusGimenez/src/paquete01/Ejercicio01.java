package paquete01;

public class Ejercicio01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(Clase1.absoluto(10.5));
		System.out.println(Clase1.absoluto(23));
		System.out.println(Clase1.redondear(10.5));
		System.out.println(Clase1.redondearAlza(10.5));
		System.out.println(Clase1.redondearBaja(10.5));
		System.out.println(Clase2.aleatorio(100));
		System.out.println(Clase2.aleatorio(20, 50));
		System.out.println(Clase2.maximo(40.5, 50.7));
		System.out.println(Clase2.minimo(40, 80));
		System.out.println(Clase2.minimo(10.50, 20.55));
	}

}
