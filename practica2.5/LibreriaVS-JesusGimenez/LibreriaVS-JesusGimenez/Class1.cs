﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_JesusGimenez
{
    public class Class1
    {
        public bool esPrimo(int n)
        {
            int cont = 0;
            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0) cont++;
            }
            if (cont == 2) return true;
            else return false;
        }
        public static bool EsPerfecto(int numero)
        {

            return numero == Factores(numero).Sum();

        }
        public static List<int> Factores(int numero)
        {

            List<int> factores = new List<int>();

            for (int i = 1; i < numero; i++)

                if (numero % i == 0) factores.Add(i);

            return factores;

        }
        public static double maximo(double a, double b)
        {
            if (a >= b)
            {
                return a;
            }
            return b;
        }

        public static int minimo(int a, int b)
        {
            if (a > b)
            {
                return b;
            }
            return a;
        }


    }

    public class Class2 {
        public static int redondear(double a)
        {

            if (a - (int)a >= 0.5)
            {
                return (int)a + 1;
            }
            return (int)a;
        }

        public static int redondearAlza(double a)
        {
            //6.1 -> 7
            //6.0 -> 6.0 

            if (a != (int)a)
            {
                return (int)a + 1;
            }
            return (int)a;

        }
        public static int redondearBaja(double a)
        {

            return (int)a;
        }
        public static int absoluto(int a)
        {
            if (a < 0)
            {
                a = -a;
            }
            return a;
        }

        public static double absoluto(double a)
        {
            if (a < 0)
            {
                a = -a;
            }
            return a;
        }
    }


}
    
