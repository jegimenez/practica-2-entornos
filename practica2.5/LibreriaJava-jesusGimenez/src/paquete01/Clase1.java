package paquete01;

public class Clase1 {
	
	public static int redondear(double a){
		
		if(a - (int)a >= 0.5){
			return (int)a + 1;
		}
		return (int)a;
	}
	
	public static int redondearAlza(double a){
		//6.1 -> 7
		//6.0 -> 6.0 
		
		if( a != (int) a ){
			return (int)a + 1;
		}
		return (int)a;
		
	}
	public static int redondearBaja(double a){
		
		return (int)a;
	}
	public static int absoluto(int a){
		if(a < 0){
			a = -a;
		}
		return a;
	}
	
	public static double absoluto(double a){
		if(a < 0){
			a = -a;
		}
		return a;
	}
	
	
}

