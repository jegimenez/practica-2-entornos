package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JSpinner;
import javax.swing.JProgressBar;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JSlider;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.SystemColor;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.UIManager;
import javax.swing.ImageIcon;
import javax.swing.JToolBar;
import java.awt.Button;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import java.awt.Toolkit;

public class VentanaPrincipal extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPrincipal frame = new VentanaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaPrincipal() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\workspace\\Eclipse-JesusGimenez\\Images\\Flecha_030.gif"));
		setTitle("Incidencia APP");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 784, 665);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("Menu.background"));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.setBounds(297, 563, 128, 42);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(159, 136, 109, 16);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(280, 133, 276, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerDateModel(new Date(1512342000000L), null, null, Calendar.DAY_OF_YEAR));
		spinner.setBounds(284, 168, 272, 22);
		contentPane.add(spinner);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 766, 26);
		contentPane.add(menuBar);
		
		JMenu mnAjustes = new JMenu("Ajustes");
		menuBar.add(mnAjustes);
		
		JMenu mnPersonalizar = new JMenu("Personalizar");
		mnAjustes.add(mnPersonalizar);
		
		JMenuItem mntmOpcin = new JMenuItem("Vista 1");
		mnPersonalizar.add(mntmOpcin);
		
		JMenuItem mntmOpcin_1 = new JMenuItem("Vista 2");
		mnPersonalizar.add(mntmOpcin_1);
		
		JMenu mnEdicin = new JMenu("Edici\u00F3n");
		menuBar.add(mnEdicin);
		
		JMenuItem mntmPlantillas = new JMenuItem("Plantillas");
		mnEdicin.add(mntmPlantillas);
		
		JMenu mnOtros = new JMenu("Otros");
		menuBar.add(mnOtros);
		
		JMenuItem mntmAyuda = new JMenuItem("Ayuda");
		mnOtros.add(mntmAyuda);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mnOtros.add(mntmSalir);
		
		JLabel lblEtiqueta = new JLabel("F. Incidencia");
		lblEtiqueta.setBounds(159, 171, 109, 16);
		contentPane.add(lblEtiqueta);
		
		JSlider slider = new JSlider();
		slider.setValue(5);
		slider.setMajorTickSpacing(1);
		slider.setMaximum(10);
		slider.setMinorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.setBounds(284, 335, 272, 52);
		contentPane.add(slider);
		
		JCheckBox chckbxElegir = new JCheckBox("Dominio");
		chckbxElegir.setSelected(true);
		chckbxElegir.setBounds(284, 273, 128, 25);
		contentPane.add(chckbxElegir);
		
		JLabel lblTtuloDeLa = new JLabel("Comunicar incidencia");
		lblTtuloDeLa.setIcon(new ImageIcon("C:\\workspace\\Eclipse-JesusGimenez\\Images\\33.png"));
		lblTtuloDeLa.setForeground(new Color(220, 20, 60));
		lblTtuloDeLa.setFont(new Font("Open Sans", Font.PLAIN, 24));
		lblTtuloDeLa.setBounds(12, 39, 388, 92);
		contentPane.add(lblTtuloDeLa);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Plan b\u00E1sico", "Plan est\u00E1ndar", "Plan premium"}));
		comboBox.setEditable(true);
		comboBox.setBounds(284, 211, 272, 22);
		contentPane.add(comboBox);
		
		JLabel lblUrgencia = new JLabel("Plan Contratado");
		lblUrgencia.setBounds(159, 214, 109, 16);
		contentPane.add(lblUrgencia);
		
		JLabel lblMensaje = new JLabel("Mensaje");
		lblMensaje.setBounds(159, 402, 109, 16);
		contentPane.add(lblMensaje);
		
		JLabel lblUrgencia_1 = new JLabel("Urgencia");
		lblUrgencia_1.setBounds(159, 333, 109, 16);
		contentPane.add(lblUrgencia_1);
		
		JLabel lblSeleccOpcin = new JLabel("Selecc. Opci\u00F3n");
		lblSeleccOpcin.setBounds(159, 277, 109, 16);
		contentPane.add(lblSeleccOpcin);
		
		JCheckBox chckbxOpcin = new JCheckBox("Hosting");
		chckbxOpcin.setBounds(428, 273, 128, 25);
		contentPane.add(chckbxOpcin);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(294, 407, 262, 125);
		contentPane.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
	}
}
