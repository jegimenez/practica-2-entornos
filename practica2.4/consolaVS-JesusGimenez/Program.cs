﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolaVS_JesusGimenez
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Introduce una opción:");
            System.Console.WriteLine("Opción 1:");
            System.Console.WriteLine("Opción 2:");
            System.Console.WriteLine("Opción 3:");
            String opcion = (Console.ReadLine());
            char a = opcion[0];
            switch (a)
            {
                case '1':
                    System.Console.WriteLine("Introduce una cadena para pasar a mayúscula");
                    String cadena = (Console.ReadLine());
                    System.Console.WriteLine(minusculaAmayuscula(cadena)); 
                    
                    break;

                case '2':
                    System.Console.WriteLine("Introduce una cadena!");
                    String cadena2 = (Console.ReadLine());
                    System.Console.WriteLine(stringReves(cadena2));

                    break;

                case '3':
                    mensaje();

                    break;
              
            }
            System.Console.ReadKey();
        }

        private static String minusculaAmayuscula(string cadena)
        {
            cadena = cadena.ToUpper();
            return cadena;
        }

        private static void mensaje()
        {
            System.Console.WriteLine("Ésto es un método VOID con un mensaje");
        }

        private static String stringReves(String cadena)
        {
            String cadena2 = "";
            for (int i = cadena.Length - 1; i >= 0; i--)
            {
                cadena2 = cadena2 + cadena[i];
            }
            return cadena2;
        }
    }
   

    
}
