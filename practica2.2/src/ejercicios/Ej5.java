package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		
		Scanner lector;
		//Leemos n�mero y inicializamos contador de divisores
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		//Bucle que se repite el mismo n�mero de veces que el n�mero introducido. Si el n�mero leido entre sus divisores es = 0, se apunta
		//un divisor.
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		//Si hay m�s de 2 divisores, no es primo, si hay menos, es primo.
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
